use std::borrow::{Borrow, Cow};
use std::str::FromStr;

use crate::{QueueResult, Receiver, Sender};

use async_trait::async_trait;
use aws_sdk_sqs::output::ReceiveMessageOutput;
use aws_sdk_sqs::{Client, Config, Credentials, Endpoint, Region};
use http::Uri;

const PROVIDER: &str = "YMQ";

#[derive(Debug)]
pub struct YMQ {
    endpoint_url: Cow<'static, str>,
    path: Cow<'static, str>,
    client: Client,
}

#[async_trait(?Send)]
impl Sender for YMQ {
    async fn send(&self, msg: impl Into<String>) -> QueueResult<()> {
        self.client
            .send_message()
            .queue_url(self.get_url())
            .message_body(msg)
            .send()
            .await?;

        Ok(())
    }
}

#[async_trait(?Send)]
impl Receiver for YMQ {
    async fn receive(&self) -> QueueResult<()> {
        let output: ReceiveMessageOutput = self
            .client
            .receive_message()
            .queue_url(self.get_url())
            .send()
            .await?;

        eprintln!("output = {:?}", output.messages());

        Ok(())
    }
}

impl YMQ {
    pub fn new<EU, P, RT, AK, SK>(
        endpoint_url: EU,
        path: P,
        region_title: RT,
        access_key: AK,
        secret_key: SK,
    ) -> Self
    where
        EU: Into<Cow<'static, str>>,
        P: Into<Cow<'static, str>>,
        RT: Into<Cow<'static, str>>,
        AK: Into<String>,
        SK: Into<String>,
    {
        let endpoint_url: Cow<str> = endpoint_url.into();
        let endpoint: Endpoint = Self::get_endpoint(endpoint_url.clone());

        Self {
            endpoint_url,
            path: path.into(),
            client: Client::from_conf(
                Config::builder()
                    .credentials_provider(Self::get_credentials(access_key, secret_key))
                    .endpoint_resolver(endpoint)
                    .region(Self::get_region(region_title))
                    .build(),
            ),
        }
    }

    fn get_credentials(
        access_key: impl Into<String>,
        secret_key: impl Into<String>,
    ) -> Credentials {
        Credentials::new(access_key, secret_key, None, None, Self::get_provider())
    }

    fn get_provider<'a>() -> &'a str {
        PROVIDER
    }

    fn get_endpoint(endpoint_url: impl Into<Cow<'static, str>>) -> Endpoint {
        Endpoint::immutable(
            Uri::from_str(endpoint_url.into().borrow()).expect("Cannot init endpoint!"),
        )
    }

    fn get_region(title: impl Into<Cow<'static, str>>) -> Region {
        Region::new(title)
    }

    fn get_url(&self) -> String {
        format!("{}/{}", self.endpoint_url, self.path)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_credentials_works() {
        let test_ak: &str = "test_ak";
        let test_sk: &str = "test_sk";

        let creds: Credentials = YMQ::get_credentials(test_ak, test_sk);

        assert_eq!(test_ak, creds.access_key_id());
        assert_eq!(test_sk, creds.secret_access_key());
    }

    #[test]
    fn get_provider_works() {
        assert_eq!(PROVIDER, YMQ::get_provider());
    }

    #[test]
    fn get_endpoint_works() {
        let test_endpoint: &str = "test.com";

        assert_eq!(
            test_endpoint,
            YMQ::get_endpoint(test_endpoint).uri().host().unwrap()
        );
    }

    #[test]
    fn get_region_works() {
        let test_region: &str = "test_region";

        assert_eq!(test_region, YMQ::get_region(test_region).to_string(),);
    }

    #[test]
    fn get_url_works() {
        let test_endpoint: &str = "test.com";
        let test_path: &str = "test_path";
        let test_region: &str = "test_region";
        let test_ak: &str = "test_ak";
        let test_sk: &str = "test_sk";

        let provider: YMQ = YMQ::new(test_endpoint, test_path, test_region, test_ak, test_sk);

        assert_eq!(format!("{test_endpoint}/{test_path}"), provider.get_url(),);
    }
}
