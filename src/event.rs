use crate::QueueResult;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "event", rename_all = "snake_case")]
pub enum Event {
    PassportLogin(ToMessage),
}

// For phone or email login
#[derive(Serialize, Deserialize, Debug)]
pub struct ToMessage {
    pub to: String,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Phone {
    pub phone: String,
}

impl Event {
    pub fn passport_login(to: impl Into<String>, msg: impl Into<String>) -> Self {
        Self::PassportLogin(ToMessage {
            to: to.into(),
            message: msg.into(),
        })
    }

    pub fn to_json(&self) -> QueueResult<String> {
        Ok(serde_json::to_string(self)?)
    }

    pub fn from_json(json: &str) -> QueueResult<Self> {
        Ok(serde_json::from_str(json)?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_phone() -> &'static str {
        "+11111111111"
    }

    fn get_msg() -> &'static str {
        "This is the test msg with phone confirmation code"
    }

    fn get_passport_login_json() -> QueueResult<String> {
        Event::passport_login(get_phone(), get_msg()).to_json()
    }

    #[test]
    fn passport_login_have_right_to_and_message() {
        match Event::passport_login(get_phone(), get_msg()) {
            Event::PassportLogin(event_data) => {
                assert_eq!(event_data.to, get_phone());
                assert_eq!(event_data.message, get_msg());
            }
        }
    }

    #[test]
    fn can_passport_login_to_json() -> QueueResult<()> {
        get_passport_login_json()?;
        Ok(())
    }

    #[test]
    fn can_passport_login_from_json() -> QueueResult<()> {
        Event::from_json(&get_passport_login_json()?)?;
        Ok(())
    }
}
