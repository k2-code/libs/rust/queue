pub mod event;
pub mod ymq;

use async_trait::async_trait;

pub type QueueResult<T> = Result<T, Box<dyn std::error::Error>>;

#[async_trait(?Send)]
pub trait Sender {
    async fn send(&self, msg: impl Into<String>) -> QueueResult<()>;
}

#[async_trait(?Send)]
pub trait Receiver {
    async fn receive(&self) -> QueueResult<()>;
}

pub struct Queue<QP>
where
    QP: Sender,
{
    qp: QP,
}

impl<QP> Queue<QP>
where
    QP: Sender,
{
    pub fn new(qp: QP) -> Self {
        Self { qp }
    }
}

impl<QP> std::ops::Deref for Queue<QP>
where
    QP: Sender,
{
    type Target = QP;

    fn deref(&self) -> &Self::Target {
        &self.qp
    }
}

#[cfg(test)]
mod test {
    use crate::ymq::YMQ;
    use crate::Queue;

    #[test]
    fn can_init_with_ymq_provider() {
        let test_endpoint: &str = "test.com";
        let test_path: &str = "test_path";
        let test_region: &str = "test_region";
        let test_ak: &str = "test_ak";
        let test_sk: &str = "test_sk";

        Queue::new(YMQ::new(
            test_endpoint,
            test_path,
            test_region,
            test_ak,
            test_sk,
        ));
    }
}
